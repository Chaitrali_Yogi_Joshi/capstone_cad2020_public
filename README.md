# Project Title

Food Ordering System

## Requirements

For development, you will only need Node.js and a node global package, installed in your environement.

### Node
- #### Node installation on Windows

Just go on [official Node.js website](https://nodejs.org/) and download the installer.
Also, be sure to have `git` available in your PATH, `npm` might need it (You can find git [here](https://git-scm.com/)).

- #### Other Operating Systems
You can find more information about the installation on the [official Node.js website](https://nodejs.org/) and the [official NPM website](https://npmjs.org/).
  
If the installation was successful, you should be able to run the following command.

    $ node --version
    v8.11.3

    $ npm --version
    6.1.0

If you need to update `npm`, you can make it using `npm`! Cool right? After running the following command, just open again the command line and be happy.

    $ npm install npm -g
	
-#### Build the solution and access website in the browser

To build the solution, please run below command.
 $ node index.js

Now go to your preferred browser and access below URL:
http://localhost:8080/

# License
This public repository uses MIT license because -
The MIT license is a great choice because it allows you to share your code under a copyleft license without forcing others to expose their proprietary code, it's business friendly and open source friendly while still allowing for monetization.